import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TypeIt from 'typeit';

class AnimatedInputBar extends Component {
  constructor(props){
    super();
    this.state = {
      text: props.value
    }
    this.strings = props.strings
    this.x = 0;
    this.y = 0;
    this.wait = 30;
    this.timespent = 0;
    this.forward = true;
    this.onChange = this.onChange.bind(this);
  }
  onChange(){

  }
  componentDidMount(){
    this.anim = setInterval(() => {
      let st = this.x === 0 ? this.state.text : this.state.text.substr(0, this.state.text.length-1);
      if (this.forward && this.timespent === 0){
        st += this.strings[this.y][this.x];
        this.x += 1;
      }else if(this.timespent === 0) {
        st = st.substr(0, st.length-1);
        this.x -= 1;
      }
      if (this.x === this.strings[this.y].length+1 && this.forward === true){
        if (this.timespent === this.wait){
          this.forward = false;
          this.timespent = 0;
        }else{
          ++this.timespent;
          return;
        }
        
      }
      if (this.x === 1 && this.forward === false){
        this.y += 1;
        this.x = 0;
        this.y = this.y % this.strings.length;
        this.forward = true;
      }
      this.setState({
        ...this.state, 
        text: this.x === 0 ? st : st + "|"
      })
    }, 50);
  }
  render() {
    return (
      <input type="text" style={{width: '700px'}}  onChange={this.onChange} value={this.state.text} ref={(ref) => this.el = ref} />
    );
  }
}
class App extends Component {
  constructor(){
    super();
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <AnimatedInputBar type="text" style={{width: '700px'}} onChange={this.onChange} value="Enter Neighbourhood" 
        strings={["Dubai Marina", "Barsha 1"]}
        ref={(ref) => this.el = ref} />
      </div>
    );
  }
}

export default App;
